# Govorko
A tool for augmentative and alternative communication.

 
Everyone has a right to a voice. This paper describes a tool for smart devices, intended to simplify communication, for those for whom speaking is a challenge every single day. The tool is aimed for children but it can be used by everyone who finds it useful. The tool is divided into categories of words. Each category contains a different number of related words, and each word is supported with an image. The user can use the tool to generate the chosen word by pressing its button. The synthesized speech is based on natural speech recordings. The communicator tool has two working modes and offers two different voices. The user can also choose whether he wants to see pictures or drawings. The tool has been made available as free software and is fully extensible thanks to its modular system architecture. In its initial version, it supports Macedonian.

[<img src="https://img.youtube.com/vi/lwpvceeLC4Y/hqdefault.jpg" width="400px"/>](https://youtu.be/lwpvceeLC4Y)

How to Install
-------
Govorko is available for free on the Google Play Store
https://play.google.com/store/apps/details?id=com.feit.ukim.edu.govorkoapp 

You can also install the app from this repo following these steps:

0. Clone this git repository.
1. Download the [Govorko Extras](http://tiny.cc/govorko_extras) and extract the files to `GovorkoApp/app/src/main/res/drawable/`
2. Download Android Studio.
3. Enable USB debugging on your [Android device](https://developer.android.com/training/basics/firstapp/running-app)
4. Start Android Studio.
5. Open GovorkoApp.
6. Connect your device to the computer via USB cable.
7. In the Android toolbar, select the device that you want to run your app on from the target device drop-down menu.
8. Click Run.

The Team
-------

The tool's development has been driven by the students studying [Computer hardware engineering and electronics](https://feit.ukim.edu.mk/en/computer-hardware-engineering-and-electronics/), at the [Faculty of Electrical Engineering and Information Technologies](http://feit.ukim.edu.mk), [Ss Cyril and Methodius University of Skopje](http://ukim.edu.mk/), Macedonia. Their work has been carried out as projects within the scope of the courses of [Electroacoustics](https://gitlab.com/feeit-freecourseware/electroacoustics), [Digital Audio Processing](https://gitlab.com/feeit-freecourseware/digital-audio-processing) and [Biomedical Electronics](https://gitlab.com/feeit-freecourseware/biomedical-electronics).

The work was supported by experts from the Association for Assistive Technologies [Open the Windows](https://openthewindows.org).

The tool was tested with the help of the Association for early stimulation, development and play in children [Kreativa Istok](https://www.facebook.com/people/Kreativa-Istok/100009641176199)
    
Publications
-------

The work has been documented in the following paper:

1. Marija Grozdanovska, Valerija Mladenovikj, Gjorgji Smilevski, Stefan Janev, Marija Velinovska Velkovska, Maja Trajchova, and Branislav Gerazov, "Towards a free software-based communication tool for children with disabilities," TELFOR 2020, Belgrade, Serbia, Nov 24 - 25, 2020. [preprint](https://www.researchgate.net/publication/345694011_Towards_a_free_software-based_communication_tool_for_children_with_disabilities)
 

License
-------
All the software is distributed with the GNU Affero General Public License v.3, included in `LICENSE`. 
The drawing and recorded audio materials are distributed under the [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) license. 


The Govorko Team

