package com.feit.ukim.edu.govorkoapp.models;

public class Choice {
    private int image;
    private String name;
    private int media;

    public Choice(String name, int image, int media) {
        this.name = name;
        this.image = image;
        this.media = media;
    }

    public int getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public int getMedia() {
        return media;
    }
}
