package com.feit.ukim.edu.govorkoapp.models;

import android.graphics.drawable.Drawable;
import android.media.Image;

public class Category {
    private CategoryTypes id;
    private String name;
    private int image;

    public Category(CategoryTypes id, String name, int image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public CategoryTypes getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

}
