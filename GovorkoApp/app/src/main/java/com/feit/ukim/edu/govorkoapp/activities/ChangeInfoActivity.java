package com.feit.ukim.edu.govorkoapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.feit.ukim.edu.govorkoapp.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ChangeInfoActivity extends AppCompatActivity {

    EditText editTextIme, editTextAdresa, editTextTel;
    Button buttonProdolzi, buttonSlika, buttonReset, settings, ok;
    LinearLayout activity_settings,activity_start;
    RadioButton buttonMashko, buttonZensko, btn_mod1, btn_mod2, btn_dete, btn_vozrasen, btn_robot, btn_simboli, btn_sliki;
    RadioGroup glas,prikaz;
    ImageView imageSlika;
    SharedPreferences pref;
    String encodedImage;
    private static int RESULT_LOAD_IMG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        editTextIme = findViewById(R.id.editTextIme);
        editTextAdresa = findViewById(R.id.editTextAdresa);
        editTextTel = findViewById(R.id.editTextTel);
        buttonMashko = findViewById(R.id.button_mashko);
        buttonZensko = findViewById(R.id.button_zensko);
        btn_mod1 = findViewById(R.id.button_mod1);
        btn_mod2 = findViewById(R.id.button_mod2);
        btn_dete = findViewById(R.id.button_dete);
        btn_vozrasen = findViewById(R.id.button_vozrasen);
        btn_robot = findViewById(R.id.button_robot);
        btn_simboli = findViewById(R.id.button_simboli);
        btn_sliki = findViewById(R.id.button_sliki);
        buttonProdolzi = findViewById(R.id.button_prodolzi);
        buttonReset = findViewById(R.id.button_reset);
        buttonSlika = findViewById(R.id.button_slika);
        imageSlika = findViewById(R.id.image_slika);
        glas = findViewById(R.id.glas);
        prikaz = findViewById(R.id.prikaz);


        activity_settings = findViewById(R.id.lay_activity_settings);
        activity_start = findViewById(R.id.lay_activity_start);
        settings = findViewById(R.id.settings);
        ok = findViewById(R.id.ok);

        pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);

        String ime = pref.getString("ime", null);
        String adresa = pref.getString("adresa", null);
        String telefon = pref.getString("telefon", null);
        encodedImage = pref.getString("slika", null);
        String pol = pref.getString("pol", null);
        String mod = pref.getString("mod", null);
        final String glas = pref.getString("glas", null);
        String prikaz = pref.getString("prikaz", null);



        if(encodedImage!=null){
            byte[] b = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            imageSlika.setImageBitmap(bitmap);
        }
        editTextIme.setText(ime);
        editTextAdresa.setText(adresa);
        editTextTel.setText(telefon);

        if(pol == "Машко"){
            buttonMashko.setChecked(true);
            buttonZensko.setChecked(false);
        }
        else if(pol == "Женско"){
            buttonMashko.setChecked(false);
            buttonZensko.setChecked(true);
        }
        if(mod == "Ниво 1"){
            btn_mod1.setChecked(true);
            btn_mod2.setChecked(false);
        }
        else if(mod == "Ниво 2"){
            btn_mod1.setChecked(false);
            btn_mod2.setChecked(true);
        }
        if(glas == "Дете"){
            btn_dete.setChecked(true);
            btn_vozrasen.setChecked(false);
            btn_robot.setChecked(false);
        }
        else if(glas == "Возрасен"){
            btn_dete.setChecked(false);
            btn_vozrasen.setChecked(true);
            btn_robot.setChecked(false);
        }
        else if(glas == "Робот"){
            btn_dete.setChecked(false);
            btn_vozrasen.setChecked(false);
            btn_robot.setChecked(true);
        }
        if(prikaz == "Симболи"){
            btn_simboli.setChecked(true);
            btn_sliki.setChecked(false);
        }
        else if(prikaz == "Слики"){
            btn_simboli.setChecked(false);
            btn_sliki.setChecked(true);
        }

        buttonSlika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });

        buttonProdolzi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor ed = pref.edit();
                ed.putString("ime", editTextIme.getText().toString());
                ed.putString("adresa", editTextAdresa.getText().toString());
                ed.putString("telefon", editTextTel.getText().toString());
                ed.putString("slika", encodedImage);
                if(buttonMashko.isChecked()==true){
                    ed.putString("pol", "Машко");
                }
                else if(buttonZensko.isChecked()==true){
                    ed.putString("pol", "Женско");
                }
                if(btn_mod1.isChecked()==true){
                    ed.putString("mod", "Ниво 1");
                }
                else if(btn_mod2.isChecked()==true){
                    ed.putString("mod", "Ниво 2");
                }
                if(btn_dete.isChecked()==true){
                    ed.putString("glas", "Дете");
                }
                else if(btn_vozrasen.isChecked()==true){
                    ed.putString("glas", "Возрасен");
                }
                else if(btn_robot.isChecked()==true){
                    ed.putString("glas", "Робот");
                }
                if(btn_simboli.isChecked()==true){
                    ed.putString("prikaz", "Симболи");
                }
                else if(btn_sliki.isChecked()==true){
                    ed.putString("prikaz", "Слики");
                }
                ed.commit();
                Intent intent = new Intent(ChangeInfoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSlika.setImageBitmap(null);
                encodedImage = null;
                editTextIme.setText("");
                editTextAdresa.setText("");
                editTextTel.setText("");
                buttonMashko.setChecked(true);
                buttonZensko.setChecked(false);
                btn_mod1.setChecked(true);
                btn_mod2.setChecked(false);
                btn_dete.setChecked(false);
                btn_vozrasen.setChecked(true);
                btn_robot.setChecked(false);
                btn_simboli.setChecked(true);
                btn_sliki.setChecked(false);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View event) {

                activity_start.setVisibility(View.INVISIBLE);
                activity_settings.setVisibility(View.VISIBLE);

                if(buttonMashko.isChecked()==true){
                    buttonMashko.setChecked(true);
                    buttonZensko.setChecked(false);
                }
                else if(buttonZensko.isChecked()==true){
                    buttonMashko.setChecked(false);
                    buttonZensko.setChecked(true);
                }
                if(btn_mod1.isChecked()==true){
                    btn_mod1.setChecked(true);
                    btn_mod2.setChecked(false);
                }
                else if(btn_mod2.isChecked()==true){
                    btn_mod1.setChecked(false);
                    btn_mod2.setChecked(true);
                }
                if(btn_dete.isChecked()==true){
                    btn_dete.setChecked(true);
                    btn_vozrasen.setChecked(false);
                    btn_robot.setChecked(false);
                }
                else if(btn_vozrasen.isChecked()==true){
                    btn_dete.setChecked(false);
                    btn_vozrasen.setChecked(true);
                    btn_robot.setChecked(false);
                }
                else if(btn_robot.isChecked()==true){
                    btn_dete.setChecked(false);
                    btn_vozrasen.setChecked(false);
                    btn_robot.setChecked(true);
                }
                if(btn_simboli.isChecked()==true){
                    btn_simboli.setChecked(true);
                    btn_sliki.setChecked(false);
                }
                else if(btn_sliki.isChecked()==true){
                    btn_simboli.setChecked(false);
                    btn_sliki.setChecked(true);
                }
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View event) {
                activity_start.setVisibility(View.VISIBLE);
                activity_settings.setVisibility(View.INVISIBLE);

            }
        });

    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Bitmap image = rotateBitmap(this, imageUri, selectedImage);
                imageSlika.setImageBitmap(image);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(ChangeInfoActivity.this, "Грешка", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(ChangeInfoActivity.this, "Одберете слика",Toast.LENGTH_LONG).show();
        }
    }

    private static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor.getCount() != 1) {
            cursor.close();
            return -1;
        }
        cursor.moveToFirst();
        int orientation = cursor.getInt(0);
        cursor.close();
        cursor = null;
        return orientation;
    }

    public static Bitmap rotateBitmap(Context context, Uri photoUri, Bitmap bitmap) {
        int orientation = getOrientation(context, photoUri);
        if (orientation <= 0) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ChangeInfoActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        if(activity_settings.getVisibility()==View.VISIBLE){
            activity_start.setVisibility(View.VISIBLE);
            activity_settings.setVisibility(View.INVISIBLE);
        }else {
            super.onBackPressed();
        }

    }
}