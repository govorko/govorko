package com.feit.ukim.edu.govorkoapp.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.feit.ukim.edu.govorkoapp.R;
import com.feit.ukim.edu.govorkoapp.activities.MainActivity;
import com.feit.ukim.edu.govorkoapp.models.Choice;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class ChoiceRecyclerViewAdapter extends RecyclerView.Adapter<ChoiceRecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "ChoiceRecyclerViewAdapter";
    Context context;

    private ArrayList<Choice> mData = new ArrayList<>();
    OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Choice item);
    }

    public void setOnAdapterItemClickListener(ChoiceRecyclerViewAdapter.OnItemClickListener clickListener) {
        this.listener = clickListener;
    }

    public ChoiceRecyclerViewAdapter(ArrayList<Choice> data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choice_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Choice choiceItem = mData.get(position);
        holder.bindItem(choiceItem);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d(TAG, "onClick: clicked on: " + mData.get(position).getName());
                listener.onItemClick(mData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            image = itemView.findViewById(R.id.image);
        }

        public void bindItem(Choice choiceItem) {

            title.setText(choiceItem.getName());
            image.setImageResource(choiceItem.getImage());
        }
    }
}