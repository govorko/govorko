package com.feit.ukim.edu.govorkoapp.models;

public enum CategoryTypes {
    TALK,
    FEEL,
    PAIN,
    FOOD_AND_DRINK,
    FAMILY,
    CLOTHES,
    ACTIVITIES,
    SCHOOL,
    KITCHEN,
    HYGIENE,
    TOYS,
    POSITION,
    QUESTIONS
}
