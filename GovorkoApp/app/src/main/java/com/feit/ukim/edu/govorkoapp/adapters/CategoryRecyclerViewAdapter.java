package com.feit.ukim.edu.govorkoapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.feit.ukim.edu.govorkoapp.R;
import com.feit.ukim.edu.govorkoapp.models.Category;


import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "CategoryRecyclerViewAdapter";

    private ArrayList<Category> mData = new ArrayList<>();
    OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Category item);
    }

    public void setOnAdapterItemClickListener(OnItemClickListener clickListener) {
        this.listener = clickListener;
    }

    public CategoryRecyclerViewAdapter(ArrayList<Category> data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Category categoryItem = mData.get(position);
        holder.bindItem(categoryItem);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " + mData.get(position).getId());
                listener.onItemClick(mData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            image = itemView.findViewById(R.id.image);
        }

        public void bindItem(Category categoryItem) {

            title.setText(categoryItem.getName());
            image.setImageResource(categoryItem.getImage());
        }
    }
}
