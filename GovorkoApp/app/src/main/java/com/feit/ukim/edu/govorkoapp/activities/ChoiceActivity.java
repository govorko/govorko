package com.feit.ukim.edu.govorkoapp.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.feit.ukim.edu.govorkoapp.R;
import com.feit.ukim.edu.govorkoapp.adapters.ChoiceRecyclerViewAdapter;
import com.feit.ukim.edu.govorkoapp.models.CategoryTypes;
import com.feit.ukim.edu.govorkoapp.models.Choice;

import java.util.ArrayList;

import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.activitiesChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.activitiesChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.activitiesChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.clothesChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.clothesChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.clothesChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.familyChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.familyChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.familyChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.feelChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.feelChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.feelChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.foodChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.foodChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.foodChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.hygieneChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.hygieneChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.hygieneChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.kitchenChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.kitchenChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.kitchenChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.painChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.painChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.painChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.positionChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.positionChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.positionChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.questionChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.questionChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.questionChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.schoolChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.schoolChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.schoolChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.talkChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.talkChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.talkChoiceMedia;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.toysChoiceDrawable;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.toysChoiceLabels;
import static com.feit.ukim.edu.govorkoapp.models.ChoicesMap.toysChoiceMedia;

public class ChoiceActivity extends AppCompatActivity {

    private static final String TAG = "ChoiceActivity";
    private ChoiceRecyclerViewAdapter mChoiceAdapter;
    private CategoryTypes mCategory;
    int voiceType = 1, imageType = 1;
    int j = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();
        mCategory = CategoryTypes.TALK;
        if (b != null) mCategory = (CategoryTypes) b.getSerializable("category");
        Log.d(TAG, "Chosen category: " + mCategory.name());
        initRecyclerView();
        playCategory(mCategory);
    }

    private void initRecyclerView() {
        ArrayList<Choice> choiceList = new ArrayList<Choice>();

        SharedPreferences sp1 = this.getSharedPreferences("ActivityPREF", MODE_PRIVATE);
        String mod = sp1.getString("mod", "Ниво 1");
        String pol = sp1.getString("pol", "Машко");
        String glas = sp1.getString("glas", "Возрасен");
        String prikaz = sp1.getString("prikaz", "Симболи");
        //voice type:
        if (pol.toString().equals("Машко")) {
            if (glas.toString().equals("Дете")) {
                voiceType = 0;
            } else if (glas.toString().equals("Возрасен")) {
                voiceType = 2;
            } else if (glas.toString().equals("Робот")) {
                voiceType = 4;
            }
        } else if (pol.toString().equals("Женско")) {
            if (glas.toString().equals("Дете")) {
                voiceType = 1;
            } else if (glas.toString().equals("Возрасен")) {
                voiceType = 3;
            } else if (glas.toString().equals("Робот")) {
                voiceType = 5;
            }
        }
        //image type:
        if (prikaz.toString().equals("Симболи")) {
            imageType = 0;
        } else if (prikaz.toString().equals("Слики")) {
            imageType = 1;
        }
        int itemsNum = 8;

        switch (mCategory) {
            case TALK:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(talkChoiceLabels[i], talkChoiceDrawable[i][imageType], talkChoiceMedia[i][voiceType]));
                }
                break;
            case FEEL:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(feelChoiceLabels[i], feelChoiceDrawable[i][imageType], feelChoiceMedia[i][voiceType]));
                }
                break;
            case PAIN:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(painChoiceLabels[i], painChoiceDrawable[i][imageType], painChoiceMedia[i][voiceType]));
                }
                break;
            case FOOD_AND_DRINK:
                for (int i = 0; i < 12;i++) {
                    choiceList.add(new Choice(foodChoiceLabels[i], foodChoiceDrawable[i][imageType], foodChoiceMedia[i][voiceType]));
                }
                break;
            case FAMILY:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(familyChoiceLabels[i], familyChoiceDrawable[i][imageType], familyChoiceMedia[i][voiceType]));
                }
                break;
            case CLOTHES:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(clothesChoiceLabels[i], clothesChoiceDrawable[i][imageType], clothesChoiceMedia[i][voiceType]));
                }
                break;
            case ACTIVITIES:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(activitiesChoiceLabels[i], activitiesChoiceDrawable[i][imageType], activitiesChoiceMedia[i][voiceType]));
                }
                break;
            case SCHOOL:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(schoolChoiceLabels[i], schoolChoiceDrawable[i][imageType], schoolChoiceMedia[i][voiceType]));
                }
                break;
            case KITCHEN:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(kitchenChoiceLabels[i], kitchenChoiceDrawable[i][imageType], kitchenChoiceMedia[i][voiceType]));
                }
                break;
            case HYGIENE:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(hygieneChoiceLabels[i], hygieneChoiceDrawable[i][imageType], hygieneChoiceMedia[i][voiceType]));
                }
                break;
            case TOYS:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(toysChoiceLabels[i], toysChoiceDrawable[i][imageType], toysChoiceMedia[i][voiceType]));
                }
                break;
            case POSITION:
                for (int i = 0; i < itemsNum;i++) {
                    choiceList.add(new Choice(positionChoiceLabels[i], positionChoiceDrawable[i][imageType], positionChoiceMedia[i][voiceType]));
                }
                break;
            case QUESTIONS:
                for (int i = 0; i < 6;i++) {
                    choiceList.add(new Choice(questionChoiceLabels[i], questionChoiceDrawable[i][imageType], questionChoiceMedia[i][voiceType]));
                }
                break;
        }

        RecyclerView recyclerView = findViewById(R.id.choice_list);
        mChoiceAdapter = new ChoiceRecyclerViewAdapter( choiceList);
        recyclerView.setAdapter(mChoiceAdapter);
        GridLayoutManager gridLayoutManager;
        gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        mChoiceAdapter.setOnAdapterItemClickListener(new ChoiceRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Choice item) {
                MediaPlayer mediaPlayer = MediaPlayer.create(ChoiceActivity.this, item.getMedia());
                mediaPlayer.start();
            }
        });

    }

    private void playCategory(CategoryTypes category) {
        MediaPlayer mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.m_da);
        SharedPreferences sp1 = this.getSharedPreferences("ActivityPREF", MODE_PRIVATE);
        String pol = sp1.getString("pol", "Машко");
        /**if (pol.toString().equals("Машко")){
         j = 0;
         }else{
         j = 1;
         }**/
        switch (category) {
            case TALK:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.razgovor);
                /**if (j==0){
                 male voice
                 }else{
                 female voice
                 }**/
                break;
            case FEEL:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.se_cuvstvuvam_z1);
                break;
            case PAIN:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.me_boli_z1);
                break;
            case FOOD_AND_DRINK:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.hrana_z1);
                break;
            case FAMILY:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.semejstvo_z1);
                break;
            case CLOTHES:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.obleka_z1);
                break;
            case ACTIVITIES:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.dejstvija);
                break;
            case SCHOOL:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.uciliste_z1);
                break;
            case KITCHEN:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.kujna_z1);
                break;
            case HYGIENE:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.higiena_z1);
                break;
            case TOYS:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.igracki_z1);
                break;
            case POSITION:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.pozicija_z1);
                break;
            case QUESTIONS:
                mediaPlayer = MediaPlayer.create(ChoiceActivity.this, R.raw.prasanja_z1);
                break;
        }
        mediaPlayer.start();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
