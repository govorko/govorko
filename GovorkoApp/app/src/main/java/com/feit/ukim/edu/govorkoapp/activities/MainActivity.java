package com.feit.ukim.edu.govorkoapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.feit.ukim.edu.govorkoapp.R;
import com.feit.ukim.edu.govorkoapp.adapters.CategoryRecyclerViewAdapter;
import com.feit.ukim.edu.govorkoapp.models.Category;
import com.feit.ukim.edu.govorkoapp.models.CategoryTypes;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.activitieChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.clothesChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.familyChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.feelChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.foodChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.hygieneChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.kitchenChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.painChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.positionChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.questionChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.schoolChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.talkChoiceImages;
import static com.feit.ukim.edu.govorkoapp.activities.CategoryMap.toysChoiceImages;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    private CategoryRecyclerViewAdapter mCategoryAdapter;

    ImageView imageSlika;
    TextView textIme, textAdresa, textTelefon, textPol, textMod, textGlas, textPrikaz;

    NavigationView navigationView;
    View headerView;
    private long mLastClickTime = 0;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);


        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        initRecyclerView();
        initComponents();
        setComponents();

    }

    private void initRecyclerView() {
        ArrayList<Category> categoryList = new ArrayList<Category>();
        SharedPreferences sp1 = this.getSharedPreferences("ActivityPREF", MODE_PRIVATE);
        String mod = sp1.getString("mod", "Ниво 1");
        String prikaz = sp1.getString("prikaz", "Симболи");
        /**if (prikaz.toString().equals("Симболи")) {
         i = 0;
         }else{
         i = 1;
         }**/
        categoryList.add(new Category(CategoryTypes.TALK, getString(R.string.talk), talkChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.FEEL, getString(R.string.feel), feelChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.PAIN, getString(R.string.pain), painChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.FOOD_AND_DRINK, getString(R.string.food_and_drink), foodChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.FAMILY, getString(R.string.family), familyChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.CLOTHES, getString(R.string.clothes), clothesChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.ACTIVITIES, getString(R.string.activities), activitieChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.SCHOOL, getString(R.string.school), schoolChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.KITCHEN, getString(R.string.kitchen), kitchenChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.HYGIENE, getString(R.string.hygiene), hygieneChoiceImages[i]));
        categoryList.add(new Category(CategoryTypes.TOYS, getString(R.string.toys), toysChoiceImages[i]));
        if (mod.toString().equals("Ниво 2")) {
            categoryList.add(new Category(CategoryTypes.POSITION, getString(R.string.position), positionChoiceImages[i]));
            categoryList.add(new Category(CategoryTypes.QUESTIONS, getString(R.string.questions), questionChoiceImages[i]));
        }

        RecyclerView recyclerView = findViewById(R.id.category_list);
        mCategoryAdapter = new CategoryRecyclerViewAdapter(categoryList);
        recyclerView.setAdapter(mCategoryAdapter);
        GridLayoutManager gridLayoutManager;
        gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        mCategoryAdapter.setOnAdapterItemClickListener(new CategoryRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Category item) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){ //preventing double click on a button
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Intent intent = new Intent(MainActivity.this, ChoiceActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("category", item.getId()); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                startActivity(intent);
                //finish();
            }
        });

    }

    public void initComponents() {
        navigationView = findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);

        imageSlika = headerView.findViewById(R.id.Slika);
        textIme = headerView.findViewById(R.id.Ime);
        textAdresa = headerView.findViewById(R.id.Adresa);
        textTelefon = headerView.findViewById(R.id.Telefon);
        textPol = headerView.findViewById(R.id.Pol);
        textMod = headerView.findViewById(R.id.Mod);
        textGlas = headerView.findViewById(R.id.Glas);
        textPrikaz = headerView.findViewById(R.id.Prikaz);

    }

    public void setComponents() {
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        SharedPreferences sp1 = this.getSharedPreferences("ActivityPREF", MODE_PRIVATE);
        String ime = sp1.getString("ime", null);
        String adresa = sp1.getString("adresa", null);
        String telefon = sp1.getString("telefon", null);
        String encodedSlika = sp1.getString("slika", null);
        String pol = sp1.getString("pol", "Машко");
        String mod = sp1.getString("mod", "Ниво 1");
        String glas = sp1.getString("glas", "Возрасен");
        String prikaz = sp1.getString("prikaz", "Симболи");
        if (encodedSlika != null) {
            byte[] b = Base64.decode(encodedSlika, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            imageSlika.setImageBitmap(bitmap);
        }
        textIme.setText(ime);
        textAdresa.setText(adresa);
        textTelefon.setText(telefon);
        textPol.setText(pol);
        textMod.setText(mod);
        textGlas.setText(glas);
        textPrikaz.setText(prikaz);



    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_promeni) {
            Intent intent = new Intent(this, ChangeInfoActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.about_me) {
            new AlertDialog.Builder(MainActivity.this)
                    .setMessage("Оваа апликација е направена од Ѓорѓи Смилевски, Валерија Младеновиќ и Марија Гроздановска, " +
                            "студенти на ФЕИТ, за организацијата " +
                            "'Отворете Ги Прозорците', во соработка со Стефан Јанев и професорите Бранислав Геразов и Христијан Ѓорески.\n" +
                            "Контакт: gerazov@feit.ukim.edu.mk")
                    .setNeutralButton("Назад", null)
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = new Intent(MainActivity.this, ChoiceActivity.class);
        Bundle b = new Bundle();
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_pocetna:
                b.putSerializable("category", CategoryTypes.TALK);
                break;
            case R.id.nav_socijalizacija:
                b.putSerializable("category", CategoryTypes.TALK);
                break;
            case R.id.nav_se_cuvstvuvam:
                b.putSerializable("category", CategoryTypes.FEEL);
                break;
            case R.id.nav_boli:
                b.putSerializable("category", CategoryTypes.PAIN);
                break;
            case R.id.nav_hrana:
                b.putSerializable("category", CategoryTypes.FOOD_AND_DRINK);
                break;
            case R.id.nav_semejstvo:
                b.putSerializable("category", CategoryTypes.FAMILY);
                break;
            case R.id.nav_obleka:
                b.putSerializable("category", CategoryTypes.CLOTHES);
                break;
            case R.id.nav_dejstvija:
                b.putSerializable("category", CategoryTypes.ACTIVITIES);
                break;
            case R.id.nav_uciliste:
                b.putSerializable("category", CategoryTypes.SCHOOL);
                break;
            case R.id.nav_higiena:
                b.putSerializable("category", CategoryTypes.HYGIENE);
                break;
            case R.id.nav_igracki:
                b.putSerializable("category", CategoryTypes.TOYS);
                break;
            case R.id.nav_pozicija:
                b.putSerializable("category", CategoryTypes.POSITION);
                break;
            case R.id.nav_prasanja:
                b.putSerializable("category", CategoryTypes.QUESTIONS);
                break;
        }

        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}