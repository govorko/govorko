package com.feit.ukim.edu.govorkoapp.activities;

import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.feit.ukim.edu.govorkoapp.R;

public class CategoryMap {

    public static final int[] talkChoiceImages = {
            R.drawable.razgovor,
            R.drawable.razgovor
    };
    public static final int[] feelChoiceImages = {
            R.drawable.se_custvuvam,
            R.drawable.se_custvuvam
    };
    public static final int[] painChoiceImages = {
            R.drawable.me_boli,
            R.drawable.me_boli
    };
    public static final int[] foodChoiceImages = {
            R.drawable.hrana_i_pijaloci,
            R.drawable.hrana_i_pijaloci
    };
    public static final int[] familyChoiceImages = {
            R.drawable.familija,
            R.drawable.familija
    };
    public static final int[] clothesChoiceImages = {
            R.drawable.obleka,
            R.drawable.obleka
    };
    public static final int[] activitieChoiceImages = {
            R.drawable.dejstvija,
            R.drawable.dejstvija
    };
    public static final int[] schoolChoiceImages = {
            R.drawable.uciliste,
            R.drawable.uciliste
    };
    public static final int[] kitchenChoiceImages = {
            R.drawable.kujna,
            R.drawable.kujna
    };
    public static final int[] hygieneChoiceImages = {
            R.drawable.higiena,
            R.drawable.higiena
    };
    public static final int[] toysChoiceImages = {
            R.drawable.igracki,
            R.drawable.igracki
    };
    public static final int[] positionChoiceImages = {
            R.drawable.pozicii,
            R.drawable.pozicii
    };
    public static final int[] questionChoiceImages = {
            R.drawable.prasanja,
            R.drawable.prasanja
    };
}
