package com.feit.ukim.edu.govorkoapp.models;

import com.feit.ukim.edu.govorkoapp.R;

/******************* Media tables content order******************/
/*****Boy***Girl***Man***Women***RobotMan***RobotWomen***/
/*array should have 8 rows and 6 columns*/
/******************* Drawable tables content order******************/
/*****Symbol***Photo***/
/*array should have 8 rows and 2 columns*/
public final class ChoicesMap {

    /********************************TALK RESOURCES*************************************************/
    public static final String[] talkChoiceLabels = {
            "ДА",
            "НЕ",
            "ЗДРАВО",
            "ЧАО",
            "ДОЈДИ",
            "МОЖЕ",
            "БЛАГОДАРАМ",
            "ТЕ МОЛАМ"
    };

    public static final int[][] talkChoiceMedia = {
            {R.raw.da_m1, R.raw.da, R.raw.da_m1, R.raw.da, R.raw.da_m1, R.raw.da},
            {R.raw.ne_m1, R.raw.ne, R.raw.ne_m1, R.raw.ne, R.raw.ne_m1, R.raw.ne},
            {R.raw.zdravo_m1, R.raw.zdravo, R.raw.zdravo_m1, R.raw.zdravo, R.raw.zdravo_m1, R.raw.zdravo},
            {R.raw.cao_m1, R.raw.cao_z1, R.raw.cao_m1, R.raw.cao_z1, R.raw.cao_m1, R.raw.cao_z1},
            {R.raw.dojdi_m1, R.raw.dojdi_z1, R.raw.dojdi_m1, R.raw.dojdi_z1, R.raw.dojdi_m1, R.raw.dojdi_z1},
            {R.raw.moze_m1, R.raw.moze_z1, R.raw.moze_m1, R.raw.moze_z1, R.raw.moze_m1, R.raw.moze_z1},
            {R.raw.blagodaram_m1, R.raw.blagodaram_z1, R.raw.blagodaram_m1, R.raw.blagodaram_z1, R.raw.blagodaram_m1, R.raw.blagodaram_z1},
            {R.raw.te_molam_m1, R.raw.te_molam_z1, R.raw.te_molam_m1, R.raw.te_molam_z1, R.raw.te_molam_m1, R.raw.te_molam_z1}
    };

    public static final int[][] talkChoiceDrawable = {
            {R.drawable.da,R.drawable.da},
            {R.drawable.ne,R.drawable.ne},
            {R.drawable.zdravo,R.drawable.zdravo},
            {R.drawable.cao,R.drawable.cao},
            {R.drawable.dojdi,R.drawable.dojdi},
            {R.drawable.moze,R.drawable.moze},
            {R.drawable.blagodaram,R.drawable.blagodaram},
            {R.drawable.te_molam,R.drawable.te_molam}

    };
    /********************************TALK RESOURCES END*************************************************/

    /********************************FEEL RESOURCES*************************************************/
    public static final String[] feelChoiceLabels = {
            "СРЕЌНО",
            "ТАЖНО",
            "ИСПЛАШЕНО",
            "НЕРВОЗНО",
            "УМОРНО",
            "УБАВО",
            "БОЛНО",
            "ЗАЉУБЕНО"
    };

    public static final int[][] feelChoiceMedia = {
            {R.raw.srekno_m1, R.raw.srekno_z1, R.raw.srekno_m1, R.raw.srekno_z1, R.raw.srekno_m1, R.raw.srekno_z1},
            {R.raw.tazno_m1, R.raw.tazno_z1, R.raw.tazno_m1, R.raw.tazno_z1, R.raw.tazno_m1, R.raw.tazno_z1},
            {R.raw.isplaseno_m1, R.raw.isplaseno_z1, R.raw.isplaseno_m1, R.raw.isplaseno_z1, R.raw.isplaseno_m1, R.raw.isplaseno_z1},
            {R.raw.nervozno_m1, R.raw.nervozno_z1, R.raw.nervozno_m1, R.raw.nervozno_z1, R.raw.nervozno_m1, R.raw.nervozno_z1},
            {R.raw.umorno_m1, R.raw.umorno_z1, R.raw.umorno_m1, R.raw.umorno_z1, R.raw.umorno_m1, R.raw.umorno_z1},
            {R.raw.ubavo_m1, R.raw.ubavo_z1, R.raw.ubavo_m1, R.raw.ubavo_z1, R.raw.ubavo_m1, R.raw.ubavo_z1},
            {R.raw.bolno_m1, R.raw.bolno_z1, R.raw.bolno_m1, R.raw.bolno_z1, R.raw.bolno_m1, R.raw.bolno_z1},
            {R.raw.zaljubeno_m1, R.raw.zaljubeno_z1, R.raw.zaljubeno_m1, R.raw.zaljubeno_z1, R.raw.zaljubeno_m1, R.raw.zaljubeno_z1}
    };

    public static final int[][] feelChoiceDrawable = {
            {R.drawable.srekjno,R.drawable.srekjno},
            {R.drawable.tazno,R.drawable.tazno},
            {R.drawable.isplaseno,R.drawable.isplaseno},
            {R.drawable.nervozno,R.drawable.nervozno},
            {R.drawable.izmoreno,R.drawable.izmoreno},
            {R.drawable.ubavo,R.drawable.ubavo},
            {R.drawable.bolno,R.drawable.bolno},
            {R.drawable.zaljubeno,R.drawable.zaljubeno},
    };
    /********************************TALK RESOURCES END*************************************************/
    /********************************PAIN RESOURCES*************************************************/
    public static final String[] painChoiceLabels = {
            "РАКА",
            "НОГА",
            "СТОМАК",
            "ГРБ",
            "ЗАБ",
            "ОКО",
            "ГЛАВА",
            "УВО"
    };

    public static final int[][] painChoiceMedia = {
            {R.raw.raka_m1, R.raw.raka_z1, R.raw.raka_m1, R.raw.raka_z1, R.raw.raka_m1, R.raw.raka_z1},
            {R.raw.noga_m1, R.raw.noga_z1, R.raw.noga_m1, R.raw.noga_z1, R.raw.noga_m1, R.raw.noga_z1},
            {R.raw.stomak_m1, R.raw.stomak_z1, R.raw.stomak_m1, R.raw.stomak_z1, R.raw.stomak_m1, R.raw.stomak_z1},
            {R.raw.grb_m1, R.raw.grb_z1, R.raw.grb_m1, R.raw.grb_z1, R.raw.grb_m1, R.raw.grb_z1},
            {R.raw.zab_m1, R.raw.zab_z1, R.raw.zab_m1, R.raw.zab_z1, R.raw.zab_m1, R.raw.zab_z1},
            {R.raw.oko_m1, R.raw.oko_z2, R.raw.oko_m1, R.raw.oko_z2, R.raw.oko_m1, R.raw.oko_z2},
            {R.raw.glava_m1, R.raw.glava_z1, R.raw.glava_m1, R.raw.glava_z1, R.raw.glava_m1, R.raw.glava_z1},
            {R.raw.uvo_m1, R.raw.uvo_z1, R.raw.uvo_m1, R.raw.uvo_z1, R.raw.uvo_m1, R.raw.uvo_z1}
    };

    public static final int[][] painChoiceDrawable = {
            {R.drawable.raka,R.drawable.raka},
            {R.drawable.noga,R.drawable.noga},
            {R.drawable.stomak,R.drawable.stomak},
            {R.drawable.grb,R.drawable.grb},
            {R.drawable.zab,R.drawable.zab},
            {R.drawable.oko,R.drawable.oko},
            {R.drawable.glava,R.drawable.glava},
            {R.drawable.uvo,R.drawable.uvo},
    };
    /********************************PAIN RESOURCES END*************************************************/
    /********************************FOOD AND DRINKS RESOURCES*************************************************/
    public static final String[] foodChoiceLabels = {
            "ДА ЈАДАМ",
            "ДА ПИЈАМ",
            "ОВОШЈЕ",
            "ЗЕЛЕНЧУК",
            "ТЕСТЕНИНИ",
            "ДЕСЕРТ",
            "ВОДА",
            "МЛЕКО",
            "СОК",
            "И",
            "САКАМ",
            "НЕ САКАМ"
    };

    public static final int[][] foodChoiceMedia = {
            {R.raw.da_jadam_m1, R.raw.da_jadam_z1, R.raw.da_jadam_m1, R.raw.da_jadam_z1, R.raw.da_jadam_m1, R.raw.da_jadam_z1},
            {R.raw.da_pijam_m1, R.raw.da_pijam_z1, R.raw.da_pijam_m1, R.raw.da_pijam_z1, R.raw.da_pijam_m1, R.raw.da_pijam_z1},
            {R.raw.ovosje_m1, R.raw.ovosje_z1, R.raw.ovosje_m1, R.raw.ovosje_z1, R.raw.ovosje_m1, R.raw.ovosje_z1},
            {R.raw.zelencuk_m1, R.raw.zelencuk_z1, R.raw.zelencuk_m1, R.raw.zelencuk_z1, R.raw.zelencuk_m1, R.raw.zelencuk_z1},
            {R.raw.testenini_m1, R.raw.testenini_z1, R.raw.testenini_m1, R.raw.testenini_z1, R.raw.testenini_m1, R.raw.testenini_z1},
            {R.raw.desert_m1, R.raw.desert_z1, R.raw.desert_m1, R.raw.desert_z1, R.raw.desert_m1, R.raw.desert_z1},
            {R.raw.voda_m1, R.raw.voda_z1, R.raw.voda_m1, R.raw.voda_z1, R.raw.voda_m1, R.raw.voda_z1},
            {R.raw.mleko_m1, R.raw.mleko_z1, R.raw.mleko_m1, R.raw.mleko_z1, R.raw.mleko_m1, R.raw.mleko_z1},
            {R.raw.sok_m1, R.raw.sok_z1, R.raw.sok_m1, R.raw.sok_z1, R.raw.sok_m1, R.raw.sok_z1},
            {R.raw.i_m1, R.raw.i_z1, R.raw.i_m1, R.raw.i_z1, R.raw.i_m1, R.raw.i_z1},
            {R.raw.sakam_m1, R.raw.sakam_z1, R.raw.sakam_m1, R.raw.sakam_z1, R.raw.sakam_m1, R.raw.sakam_z1},
            {R.raw.ne_sakam_m1, R.raw.ne_sakam_z1, R.raw.ne_sakam_m1, R.raw.ne_sakam_z1, R.raw.ne_sakam_m1, R.raw.ne_sakam_z1}
    };

    public static final int[][] foodChoiceDrawable = {
            {R.drawable.da_jadam,R.drawable.da_jadam},
            {R.drawable.da_pijam,R.drawable.da_pijam},
            {R.drawable.ovosje,R.drawable.ovosje},
            {R.drawable.zelencuk,R.drawable.zelencuk},
            {R.drawable.testenini,R.drawable.testenini},
            {R.drawable.desert,R.drawable.desert},
            {R.drawable.voda,R.drawable.voda},
            {R.drawable.mleko,R.drawable.mleko},
            {R.drawable.sok,R.drawable.sok},
            {R.drawable.i,R.drawable.i},
            {R.drawable.sakam,R.drawable.sakam},
            {R.drawable.ne_sakam,R.drawable.ne_sakam}
    };
    /********************************FOOD AND DRINKS RESOURCES END*************************************************/
    /********************************FAMILY RESOURCES*************************************************/
    public static final String[] familyChoiceLabels = {
            "МАЈКА",
            "ТАТКО",
            "БРАТ",
            "СЕСТРА",
            "БРАТУЧЕДИ",
            "БАБА",
            "ДЕДО",
            "ПРИЈАТЕЛИ"
    };

    public static final int[][] familyChoiceMedia = {
            {R.raw.majka_m1, R.raw.majka_z1, R.raw.majka_m1, R.raw.majka_z1, R.raw.majka_m1, R.raw.majka_z1},
            {R.raw.tatko_m1, R.raw.tatko_z1, R.raw.tatko_m1, R.raw.tatko_z1, R.raw.tatko_m1, R.raw.tatko_z1},
            {R.raw.brat_m1, R.raw.brat_z1, R.raw.brat_m1, R.raw.brat_z1, R.raw.brat_m1, R.raw.brat_z1},
            {R.raw.sestra_m1, R.raw.sestra_z1, R.raw.sestra_m1, R.raw.sestra_z1, R.raw.sestra_m1, R.raw.sestra_z1},
            {R.raw.bratucedi_m1, R.raw.bratucedi_z1, R.raw.bratucedi_m1, R.raw.bratucedi_z1, R.raw.bratucedi_m1, R.raw.bratucedi_z1},
            {R.raw.baba_m1, R.raw.baba_z1, R.raw.baba_m1, R.raw.baba_z1, R.raw.baba_m1, R.raw.baba_z1},
            {R.raw.dedo_m1, R.raw.dedo_z1, R.raw.dedo_m1, R.raw.dedo_z1, R.raw.dedo_m1, R.raw.dedo_z1},
            {R.raw.prijateli_m1, R.raw.prijateli_z1, R.raw.prijateli_m1, R.raw.prijateli_z1, R.raw.prijateli_m1, R.raw.prijateli_z1}
    };

    public static final int[][] familyChoiceDrawable = {
            {R.drawable.majka,R.drawable.majka},
            {R.drawable.tatko,R.drawable.tatko},
            {R.drawable.brat,R.drawable.brat},
            {R.drawable.sestra,R.drawable.sestra},
            {R.drawable.bratucedi,R.drawable.bratucedi},
            {R.drawable.baba,R.drawable.baba},
            {R.drawable.dedo,R.drawable.dedo},
            {R.drawable.prijateli,R.drawable.prijateli},
    };
    /********************************FAMILY RESOURCES END*************************************************/
    /********************************CLOTHES RESOURCES*************************************************/
    public static final String[] clothesChoiceLabels = {
            "БЛУЗА",
            "ПАНТОЛОНИ",
            "ЧЕВЛИ",
            "КАПА",
            "ЧОРАПИ",
            "ПИЖАМИ",
            "ЈАКНА",
            "КАИШ"
    };

    public static final int[][] clothesChoiceMedia = {
            {R.raw.bluza_m1, R.raw.bluza_z1, R.raw.bluza_m1, R.raw.bluza_z1, R.raw.bluza_m1, R.raw.bluza_z1},
            {R.raw.pantoloni_m1, R.raw.pantoloni_z1, R.raw.pantoloni_m1, R.raw.pantoloni_z1, R.raw.pantoloni_m1, R.raw.pantoloni_z1},
            {R.raw.cevli_m1, R.raw.cevli_z1, R.raw.cevli_m1, R.raw.cevli_z1, R.raw.cevli_m1, R.raw.cevli_z1},
            {R.raw.kapa_m1, R.raw.kapa_z1, R.raw.kapa_m1, R.raw.kapa_z1, R.raw.kapa_m1, R.raw.kapa_z1},
            {R.raw.corapi_m1, R.raw.corapi_z1, R.raw.corapi_m1, R.raw.corapi_z1, R.raw.corapi_m1, R.raw.corapi_z1},
            {R.raw.pizami_m1, R.raw.pizami_z1, R.raw.pizami_m1, R.raw.pizami_z1, R.raw.pizami_m1, R.raw.pizami_z1},
            {R.raw.jakna_m1, R.raw.jakna_z1, R.raw.jakna_m1, R.raw.jakna_z1, R.raw.jakna_m1, R.raw.jakna_z1},
            {R.raw.kais_m1, R.raw.kais_z1, R.raw.kais_m1, R.raw.kais_z1, R.raw.kais_m1, R.raw.kais_z1}
    };

    public static final int[][] clothesChoiceDrawable = {
            {R.drawable.bluza,R.drawable.bluza},
            {R.drawable.pantoloni,R.drawable.pantoloni},
            {R.drawable.patiki,R.drawable.patiki},
            {R.drawable.kapa,R.drawable.kapa},
            {R.drawable.corapi,R.drawable.corapi},
            {R.drawable.pizami,R.drawable.pizami},
            {R.drawable.jakna,R.drawable.jakna},
            {R.drawable.kais,R.drawable.kais},
    };
    /********************************CLOTHES RESOURCES END*************************************************/
    /********************************ACTIVITIES RESOURCES*************************************************/
    public static final String[] activitiesChoiceLabels = {
            "СЛУШАМ",
            "ОДАМ",
            "ЧИТАМ",
            "ЧЕКАМ",
            "ГЛЕДАМ",
            "ДА СПИЈАМ",
            "УЧАМ",
            "СЕ ДОПИШУВАМ"
    };

    public static final int[][] activitiesChoiceMedia = {
            {R.raw.slusam_m1, R.raw.slusam_z1, R.raw.slusam_m1, R.raw.slusam_z1, R.raw.slusam_m1, R.raw.slusam_z1},
            {R.raw.odam_m1, R.raw.odam_z1, R.raw.odam_m1, R.raw.odam_z1, R.raw.odam_m1, R.raw.odam_z1},
            {R.raw.citam_m1, R.raw.citam_z1, R.raw.citam_m1, R.raw.citam_z1, R.raw.citam_m1, R.raw.citam_z1},
            {R.raw.cekam_m1, R.raw.cekam_z1, R.raw.cekam_m1, R.raw.cekam_z1, R.raw.cekam_m1, R.raw.cekam_z1},
            {R.raw.gledam_m1, R.raw.gledam_z1, R.raw.gledam_m1, R.raw.gledam_z1, R.raw.gledam_m1, R.raw.gledam_z1},
            {R.raw.da_spijam_m1, R.raw.da_spijam_z1, R.raw.da_spijam_m1, R.raw.da_spijam_z1, R.raw.da_spijam_m1, R.raw.da_spijam_z1},
            {R.raw.ucam_m1, R.raw.ucam_z1, R.raw.ucam_m1, R.raw.ucam_z1, R.raw.ucam_m1, R.raw.ucam_z1},
            {R.raw.se_dopisuvam_m1, R.raw.se_dopisuvam_z1, R.raw.se_dopisuvam_m1, R.raw.se_dopisuvam_z1, R.raw.se_dopisuvam_m1, R.raw.se_dopisuvam_z1}
    };

    public static final int[][] activitiesChoiceDrawable = {
            {R.drawable.slusam,R.drawable.slusam},
            {R.drawable.da_odam,R.drawable.da_odam},
            {R.drawable.citam,R.drawable.citam},
            {R.drawable.cekam,R.drawable.cekam},
            {R.drawable.gledam,R.drawable.gledam},
            {R.drawable.da_spijam,R.drawable.da_spijam},
            {R.drawable.ucam,R.drawable.ucam},
            {R.drawable.se_dopisuvam,R.drawable.se_dopisuvam},
    };
    /********************************ACTIVITIES RESOURCES END*************************************************/
    /********************************SCHOOL RESOURCES*************************************************/
    public static final String[] schoolChoiceLabels = {
            "МОЛИВ",
            "ГУМА",
            "ТАБЛА",
            "КНИГА",
            "КАЛКУЛАТОР",
            "ТЕТРАТКА",
            "РАНЕЦ",
            "НАСТАВНИК"
    };

    public static final int[][] schoolChoiceMedia = {
            {R.raw.moliv_m1, R.raw.moliv_z1, R.raw.moliv_m1, R.raw.moliv_z1, R.raw.moliv_m1, R.raw.moliv_z1},
            {R.raw.guma_m1, R.raw.guma_z1, R.raw.guma_m1, R.raw.guma_z1, R.raw.guma_m1, R.raw.guma_z1},
            {R.raw.tabla_m1, R.raw.tabla_z1, R.raw.tabla_m1, R.raw.tabla_z1, R.raw.tabla_m1, R.raw.tabla_z1},
            {R.raw.kniga_m1, R.raw.kniga_z1, R.raw.kniga_m1, R.raw.kniga_z1, R.raw.kniga_m1, R.raw.kniga_z1},
            {R.raw.kalkulator_m1, R.raw.kalkulator_z1, R.raw.kalkulator_m1, R.raw.kalkulator_z1, R.raw.kalkulator_m1, R.raw.kalkulator_z1},
            {R.raw.tetratka_m1, R.raw.tetratka_z1, R.raw.tetratka_m1, R.raw.tetratka_z1, R.raw.tetratka_m1, R.raw.tetratka_z1},
            {R.raw.ranec_m1, R.raw.ranec_z1, R.raw.ranec_m1, R.raw.ranec_z1, R.raw.ranec_m1, R.raw.ranec_z1},
            {R.raw.nastavnik_m1, R.raw.nastavnik_z1, R.raw.nastavnik_m1, R.raw.nastavnik_z1, R.raw.nastavnik_m1, R.raw.nastavnik_z1}
    };

    public static final int[][] schoolChoiceDrawable = {
            {R.drawable.moliv,R.drawable.moliv},
            {R.drawable.guma,R.drawable.guma},
            {R.drawable.tabla,R.drawable.tabla},
            {R.drawable.kniga,R.drawable.kniga},
            {R.drawable.kalkulator,R.drawable.kalkulator},
            {R.drawable.tetratka,R.drawable.tetratka},
            {R.drawable.ranec,R.drawable.ranec},
            {R.drawable.nastavnik,R.drawable.nastavnik},
    };
    /********************************SCHOOL RESOURCES END*************************************************/
    /********************************KITCHEN RESOURCES*************************************************/
    public static final String[] kitchenChoiceLabels = {
            "КАДЕ Е",
            "МИ ТРЕБА",
            "ДАЈ МИ",
            "ЧИНИЈА",
            "ЛАЖИЦА",
            "ВИЛУШКА",
            "НОЖ",
            "ФРИЖИДЕР"
    };

    public static final int[][] kitchenChoiceMedia = {
            {R.raw.kade_e_m1, R.raw.kade_e_z1, R.raw.kade_e_m1, R.raw.kade_e_z1, R.raw.kade_e_m1, R.raw.kade_e_z1},
            {R.raw.mi_treba_m1, R.raw.mi_treba_z1, R.raw.mi_treba_m1, R.raw.mi_treba_z1, R.raw.mi_treba_m1, R.raw.mi_treba_z1},
            {R.raw.daj_mi_m1, R.raw.daj_mi_z1, R.raw.daj_mi_m1, R.raw.daj_mi_z1, R.raw.daj_mi_m1, R.raw.daj_mi_z1},
            {R.raw.cinija_m1, R.raw.cinija_z1, R.raw.cinija_m1, R.raw.cinija_z1, R.raw.cinija_m1, R.raw.cinija_z1},
            {R.raw.lazica_m1, R.raw.lazica_z1, R.raw.lazica_m1, R.raw.lazica_z1, R.raw.lazica_m1, R.raw.lazica_z1},
            {R.raw.viluska_m1, R.raw.viluska_z1, R.raw.viluska_m1, R.raw.viluska_z1, R.raw.viluska_m1, R.raw.viluska_z1},
            {R.raw.noz_m1, R.raw.noz_z1, R.raw.noz_m1, R.raw.noz_z1, R.raw.noz_m1, R.raw.noz_z1},
            {R.raw.frizider_m1, R.raw.frizider_z1, R.raw.frizider_m1, R.raw.frizider_z1, R.raw.frizider_m1, R.raw.frizider_z1}
    };

    public static final int[][] kitchenChoiceDrawable = {
            {R.drawable.kade_e,R.drawable.kade_e},
            {R.drawable.mi_treba,R.drawable.mi_treba},
            {R.drawable.daj_mi,R.drawable.daj_mi},
            {R.drawable.cinija,R.drawable.cinija},
            {R.drawable.lazica,R.drawable.lazica},
            {R.drawable.viluska,R.drawable.viluska},
            {R.drawable.noz,R.drawable.noz},
            {R.drawable.frizider,R.drawable.frizider},
    };
    /********************************KITCHEN RESOURCES END*************************************************/
    /********************************HYGIENE RESOURCES*************************************************/
    public static final String[] hygieneChoiceLabels = {
            "ЧЕТКА ЗА ЗАБИ",
            "ПАСТА",
            "ПЕШКИР",
            "ТОАЛЕТ ХАРТИЈА",
            "ВЛОШКА",
            "ФЕН",
            "САПУН",
            "ЧЕШЕЛ"
    };

    public static final int[][] hygieneChoiceMedia = {
            {R.raw.cetka_za_zabi_m1, R.raw.cetka_za_zabi_z1, R.raw.cetka_za_zabi_m1, R.raw.cetka_za_zabi_z1, R.raw.cetka_za_zabi_m1, R.raw.cetka_za_zabi_z1},
            {R.raw.pasta_m1, R.raw.pasta_z1, R.raw.pasta_m1, R.raw.pasta_z1, R.raw.pasta_m1, R.raw.pasta_z1},
            {R.raw.peskir_m1, R.raw.peskir_z1, R.raw.peskir_m1, R.raw.peskir_z1, R.raw.peskir_m1, R.raw.peskir_z1},
            {R.raw.toalet_hartija_m1, R.raw.toalet_hartija_z1, R.raw.toalet_hartija_m1, R.raw.toalet_hartija_z1, R.raw.toalet_hartija_m1, R.raw.toalet_hartija_z1},
            {R.raw.vloska_m1, R.raw.vloska_z1, R.raw.vloska_m1, R.raw.vloska_z1, R.raw.vloska_m1, R.raw.vloska_z1},
            {R.raw.fen_m1, R.raw.fen_z1, R.raw.fen_m1, R.raw.fen_z1, R.raw.fen_m1, R.raw.fen_z1},
            {R.raw.sapun_m1, R.raw.sapun_z1, R.raw.sapun_m1, R.raw.sapun_z1, R.raw.sapun_m1, R.raw.sapun_z1},
            {R.raw.cesel_m1, R.raw.cesel_z1, R.raw.cesel_m1, R.raw.cesel_z1, R.raw.cesel_m1, R.raw.cesel_z1}
    };

    public static final int[][] hygieneChoiceDrawable = {
            {R.drawable.cetka_zabi,R.drawable.cetka_zabi},
            {R.drawable.pasta,R.drawable.pasta},
            {R.drawable.krpa,R.drawable.krpa},
            {R.drawable.toalet,R.drawable.toalet},
            {R.drawable.vloska,R.drawable.vloska},
            {R.drawable.fen,R.drawable.fen},
            {R.drawable.sapun,R.drawable.sapun},
            {R.drawable.cesel,R.drawable.cesel},
    };
    /********************************HYGIENE RESOURCES END*************************************************/
    /********************************TOYS RESOURCES*************************************************/
    public static final String[] toysChoiceLabels = {
            "САКАМ",
            "ИМАМ",
            "КАДЕ Е",
            "КОЦКИ",
            "КУКЛА",
            "АВТОМОБИЛ",
            "БАЛОНИ",
            "ТОПКА"
    };

    public static final int[][] toysChoiceMedia = {
            {R.raw.sakam_m1, R.raw.sakam_z1, R.raw.sakam_m1, R.raw.sakam_z1, R.raw.sakam_m1, R.raw.sakam_z1},
            {R.raw.imam_m1, R.raw.imam_z1, R.raw.imam_m1, R.raw.imam_z1, R.raw.imam_m1, R.raw.imam_z1},
            {R.raw.kade_e_m1, R.raw.kade_e_z1, R.raw.kade_e_m1, R.raw.kade_e_z1, R.raw.kade_e_m1, R.raw.kade_e_z1},
            {R.raw.kocki_m1, R.raw.kocki_z1, R.raw.kocki_m1, R.raw.kocki_z1, R.raw.kocki_m1, R.raw.kocki_z1},
            {R.raw.kukla_m1, R.raw.kukla_z1, R.raw.kukla_m1, R.raw.kukla_z1, R.raw.kukla_m1, R.raw.kukla_z1},
            {R.raw.avtomobil_m1, R.raw.avtomobil_z1, R.raw.avtomobil_m1, R.raw.avtomobil_z1, R.raw.avtomobil_m1, R.raw.avtomobil_z1},
            {R.raw.baloni_m1, R.raw.baloni_z1, R.raw.baloni_m1, R.raw.baloni_z1, R.raw.baloni_m1, R.raw.baloni_z1},
            {R.raw.topka_m1, R.raw.topka_z1, R.raw.topka_m1, R.raw.topka_z1, R.raw.topka_m1, R.raw.topka_z1}
    };

    public static final int[][] toysChoiceDrawable = {
            {R.drawable.sakam2,R.drawable.sakam2},
            {R.drawable.imam,R.drawable.imam},
            {R.drawable.kade_e,R.drawable.kade_e},
            {R.drawable.kocki,R.drawable.kocki},
            {R.drawable.kukla,R.drawable.kukla},
            {R.drawable.kola,R.drawable.kola},
            {R.drawable.baloni,R.drawable.baloni},
            {R.drawable.topka,R.drawable.topka},
    };
    /********************************TOYS RESOURCES END*************************************************/
    /********************************POSITION RESOURCES*************************************************/
    public static final String[] positionChoiceLabels = {
            "ГОРЕ",
            "ДОЛУ",
            "ЛЕВО",
            "ДЕСНО",
            "НАПРЕД",
            "НАЗАД",
            "ВО",
            "НА"
    };

    public static final int[][] positionChoiceMedia = {
            {R.raw.gore_m1, R.raw.gore_z1, R.raw.gore_m1, R.raw.gore_z1, R.raw.gore_m1, R.raw.gore_z1},
            {R.raw.dolu_m1, R.raw.dolu_z1, R.raw.dolu_m1, R.raw.dolu_z1, R.raw.dolu_m1, R.raw.dolu_z1},
            {R.raw.levo_m1, R.raw.levo_z1, R.raw.levo_m1, R.raw.levo_z1, R.raw.levo_m1, R.raw.levo_z1},
            {R.raw.desno_m1, R.raw.desno_z1, R.raw.desno_m1, R.raw.desno_z1, R.raw.desno_m1, R.raw.desno_z1},
            {R.raw.napred_m1, R.raw.napred_z1, R.raw.napred_m1, R.raw.napred_z1, R.raw.napred_m1, R.raw.napred_z1},
            {R.raw.nazad_m1, R.raw.nazad_z1, R.raw.nazad_m1, R.raw.nazad_z1, R.raw.nazad_m1, R.raw.nazad_z1},
            {R.raw.vo_m1, R.raw.vo_z1, R.raw.vo_m1, R.raw.vo_z1, R.raw.vo_m1, R.raw.vo_z1},
            {R.raw.na_m1, R.raw.na_z1, R.raw.na_m1, R.raw.na_z1, R.raw.na_m1, R.raw.na_z1}
    };

    public static final int[][] positionChoiceDrawable = {
            {R.drawable.gore,R.drawable.gore},
            {R.drawable.dolu,R.drawable.dolu},
            {R.drawable.levo,R.drawable.levo},
            {R.drawable.desno,R.drawable.desno},
            {R.drawable.napred,R.drawable.napred},
            {R.drawable.nazad,R.drawable.nazad},
            {R.drawable.vo,R.drawable.vo},
            {R.drawable.na,R.drawable.na},
    };
    /********************************POSITION RESOURCES END*************************************************/
    /********************************QUESTIONS RESOURCES*************************************************/
    public static final String[] questionChoiceLabels = {
            "КОЈ",
            "КАДЕ",
            "ШТО",
            "ЗОШТО",
            //"ДАЛИ",
            "КОЛКУ",
            "КОМУ",
            //"ЧИЈ"
    };

    public static final int[][] questionChoiceMedia = {
            {R.raw.koj_m1, R.raw.koj_z1, R.raw.koj_m1, R.raw.koj_z1, R.raw.koj_m1, R.raw.koj_z1},
            {R.raw.kade_m1, R.raw.kade_z1, R.raw.kade_m1, R.raw.kade_z1, R.raw.kade_m1, R.raw.kade_z1},
            {R.raw.sto_m1, R.raw.sto_z1, R.raw.sto_m1, R.raw.sto_z1, R.raw.sto_m1, R.raw.sto_z1},
            {R.raw.zosto_m1, R.raw.zosto_z1, R.raw.zosto_m1, R.raw.zosto_z1, R.raw.zosto_m1, R.raw.zosto_z1},
            {R.raw.kolku_m1, R.raw.kolku_z1, R.raw.kolku_m1, R.raw.kolku_z1, R.raw.kolku_m1, R.raw.kolku_z1},
            {R.raw.komu_m1, R.raw.komu_z1, R.raw.komu_m1, R.raw.komu_z1, R.raw.komu_m1, R.raw.komu_z1},
            //{R.raw.m_bolno, R.raw.z_bolno, R.raw.m_bolno, R.raw.z_bolno, R.raw.m_bolno, R.raw.z_bolno},
            //{R.raw.m_bolno, R.raw.z_bolno, R.raw.m_bolno, R.raw.z_bolno, R.raw.m_bolno, R.raw.z_bolno}
    };

    public static final int[][] questionChoiceDrawable = {
            {R.drawable.koj,R.drawable.koj},
            {R.drawable.kade_e2,R.drawable.kade_e2},
            {R.drawable.sto,R.drawable.sto},
            {R.drawable.zosto,R.drawable.zosto},
            //{R.drawable.ucam,R.drawable.ucam},
            {R.drawable.kolku,R.drawable.kolku},
            {R.drawable.koe,R.drawable.koe},
            //{R.drawable.ucam,R.drawable.ucam},
    };
    /********************************QUESTIONS RESOURCES END*************************************************/

}
